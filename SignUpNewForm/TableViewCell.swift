//
//  TableViewCell.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var lblSubject: UILabel!
    
    @IBOutlet weak var circle: UIView!
    
    @IBOutlet weak var lblTimeDuration: UILabel!
    
    @IBOutlet weak var lblEnrolled: UILabel!
   
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var lblperStudent: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.stackView.layer.borderWidth = 1
        self.circle.layer.cornerRadius = circle.frame.size.width/2
//        self.stackView.layer.borderColor =
    }

    
    

}
