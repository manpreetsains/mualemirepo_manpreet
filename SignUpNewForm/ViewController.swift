////
////  ViewController.swift
////  SignUpNewForm
////
////  Created by Sierra 4 on 23/02/17.
////  Copyright © 2017 Sierra 4. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import ObjectMapper
//import NVActivityIndicatorView
//import CoreData
//
//class ViewController: UIViewController {
//    
//    @IBOutlet weak var txtUserName: UITextField!
//    
//    @IBOutlet weak var txtPassword: UITextField!
//    
//    @IBOutlet weak var btnCheckBox: UIButton!
//    
//    var username: String?
//    var phone: String?
//    var email: String?
//    var password: String?
//    var country:String?
//    var city:String?
//    var address:String?
//    var flag = true
//    var indicator:NVActivityIndicatorView?
//    var datasave = [SignUp]()
//    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//    
//    
//    var checked = UIImage(named: "checked")
//    
//    var uncheck = UIImage(named: "uncheck")
//    
//    var isBoxClicked = Bool()
//    
//    override func viewDidLoad() {
//        
//        super.viewDidLoad()
//        
//        indicator = NVActivityIndicatorView(frame:CGRect.init(x: self.view.center.x-30,y:self.view.center.y+200,width:40,height:40),type: .lineSpinFadeLoader,color: UIColor.gray, padding: 5)
//        let color = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 0.8)
//        
//        txtUserName.attributedPlaceholder = NSAttributedString(string: "Username",
//                                                               attributes: [NSForegroundColorAttributeName: color ])
//        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
//                                                               attributes: [NSForegroundColorAttributeName: color ])
//        btnCheckBox.setImage(uncheck, for: .normal)
//        
//        
//    }
//  
//
//
//    @IBAction func btnSignIn(_ sender: Any) {
//        
//       
//        if((txtUserName.text?.isEmpty)!)
//        {
//           alertControl(alertDisplay: "Enter Email")
//            flag=false
//        }
//       
//        if((txtPassword.text?.isEmpty)!)
//        {
//             alertControl(alertDisplay: "Enter PassWord")
//            flag=false
//        }
//        if flag == true
//        {
//            show()
//        fetchData()
//        
//        }
//    }
//    func show() {
//        self.view.addSubview(indicator!)
//        UIView.animate(withDuration: 0.2, animations: {
//            UIApplication.shared.keyWindow?.addSubview(self.indicator!)
//            self.indicator?.startAnimating()
//        })
//    }
//    func hide() {
//        indicator?.stopAnimating()
//        UIView.animate(withDuration: 0.2, animations: {
//            self.indicator?.removeFromSuperview()
//            self.view.removeFromSuperview()
//        })
//    }
//    
//    func fetchData() {
//        
//        let param:[String:Any] = ["email": txtUserName.text!, "password":txtPassword.text!,"flag":"1"]
//        
//        ApiHandler.fetchData(urlStr: "login", parameters: param)
//        { (jsonData) in
//            print(jsonData!)
//            let userModel = Mapper<User>().map(JSONObject: jsonData)
//            
//            print(userModel?.msg ?? "")
//            print(userModel?.profile?.username ?? "")
//            print(userModel?.profile?.phone ?? "")
//            
//            self.username = userModel?.profile?.username ?? ""
//            self.email = userModel?.profile?.email ?? ""
//            self.password = self.txtPassword.text
//            self.country = userModel?.profile?.country ?? ""
//            self.city = userModel?.profile?.city ?? ""
//            self.address = userModel?.profile?.address ?? ""
//            self.phone = userModel?.profile?.phone ?? ""
//            self.performSegue(withIdentifier: "pass", sender:self )
//            self.hide()
//        }
//        
//    }
//    
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
//    {
//        
//        if segue.identifier == "pass"
//        {
//            let yourNextViewcontroller = (segue.destination as! UserDetailsViewController)
//            yourNextViewcontroller.username2 = self.username!
//            yourNextViewcontroller.password2 = self.password!
//            yourNextViewcontroller.phone2 = self.phone!
//            yourNextViewcontroller.email2 = self.email!
//            yourNextViewcontroller.country2 = self.country!
//            yourNextViewcontroller.city2 = self.city!
//            yourNextViewcontroller.address2 = self.address!
//        }
//    }
//    
//    func btnCheckBoxAction(_ sender: Any) {
//        
//        if isBoxClicked == true{
//            
//            isBoxClicked = false
//        }
//        else
//        {
//            isBoxClicked = true
//        }
//        
//        if isBoxClicked == true
//        {
//            btnCheckBox.setImage(checked, for: .normal)
//        }
//        else
//        {
//            btnCheckBox.setImage(uncheck, for: .normal)
//            
//        }
//        
//    }
//    
//    func alertControl(alertDisplay:String)
//    {
//        let alertController = UIAlertController(title: "Alert", message: alertDisplay, preferredStyle: UIAlertControllerStyle.alert)
//        
//        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//        
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//    
//    
//    
//    
//}
//
