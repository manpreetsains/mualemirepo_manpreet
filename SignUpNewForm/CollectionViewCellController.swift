//
//  CollectionViewCellController.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class CollectionViewCellController: UICollectionViewCell {
    
    @IBOutlet weak var tableView: UITableView!
    
    var details = [Details]()
    var iData = [IData]()
    var isubjects = [Subjects]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
    }
    
//    func setupTableView(idata : IData?) {
//        self.Data = idata
//        tableView.reloadData()
//        
//    }

    
}
extension CollectionViewCellController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return details.count
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
//        print( Data?[indexPath.section].subjects?[indexPath.row].subject_name ?? "")
        
        cell.lblEnrolled.text="\(details[indexPath.row].enrolled ?? 0)"
        cell.lblperStudent.text=details[indexPath.row].charge_student
        cell.lblAge.text=details[indexPath.row].age_group
        
        cell.lblSubject.text=(details[indexPath.row].subjects?[0].subject_name)! + " \(indexPath.row + 1)"
        cell.lblHome.text="Home  "+details[indexPath.row].day_location!
        cell.lblTime.text=details[indexPath.row].start_time1
        cell.lblTimeDuration.text=details[indexPath.row].time_duration
      
//         cell.lblSubject.text=details[indexPath.row].subjects?[0].subject_name
//        cell.lblAddress.text = Data?[indexPath.row].day_location
        return cell
    }
    
   
    
}
