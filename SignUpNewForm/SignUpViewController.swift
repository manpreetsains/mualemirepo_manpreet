////
////  SignUpViewController.swift
////  SignUpNewForm
////
////  Created by Sierra 4 on 23/02/17.
////  Copyright © 2017 Sierra 4. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import ObjectMapper
//
//
//class SignUpViewController: UIViewController,UITextFieldDelegate {
//    
//    
//    @IBOutlet weak var txtName: UITextField!
//    
//    @IBOutlet weak var txtEmail: UITextField!
//    
//    
//    @IBOutlet weak var txtPassword: UITextField!
//    
//    
//    @IBOutlet weak var txtPhoneNo: UITextField!
//    
//    
//    @IBOutlet weak var txtCountry: UITextField!
//    
//    @IBOutlet weak var txtCity: UITextField!
//    
//    @IBOutlet weak var address: UITextField!
//    
//    var validCheck  = Bool()
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view.
//        
//        bottomBorder(textFieldName: txtPassword,placeHolder: "Password")
//        bottomBorder(textFieldName: txtCity,placeHolder:"City")
//        bottomBorder(textFieldName: txtName,placeHolder:"Name")
//        bottomBorder(textFieldName: txtEmail,placeHolder:"Email Address")
//        bottomBorder(textFieldName: address,placeHolder:"Address")
//        bottomBorder(textFieldName: txtPhoneNo,placeHolder:"Phone No")
//        bottomBorder(textFieldName: txtCountry,placeHolder:"Country")
//        
//        
//        
//    }
//  
//
//    
//    @IBAction func btnSignUpAction(_ sender: Any) {
//        checkingAllTextFields()
//        
//        fetchData()
//        alertControl(alertDisplay: "SignUp Successfull")
//    }
// 
//    
////    username
////    email
////    password
////    phone
////    country
////    city
////    address
////    flag = 1
////    birthday(FORMAT - m/d/Y - 04/06/1993)
////    country_code
////    postal_code
////    country_iso3
////    state
//    
//    func fetchData() {
//        
//        let param:[String:Any] = ["username": txtName.text!,"email":txtEmail.text!, "password":txtPassword.text!, "phone":txtPhoneNo.text!,"country":txtCountry.text!,"city":txtCity.text!,"address":address.text ?? "0","birthday":"04/29/2017", "flag":"1"]
//        
//        ApiHandler.fetchData(urlStr: "signup", parameters: param) { (jsonData) in
//             print(jsonData)
//            let userModel = Mapper<User>().map(JSONObject: jsonData)
//            
//            print(userModel?.msg ?? "")
//            print(userModel?.profile?.username ?? "")
//            print(userModel?.profile?.phone ?? "")
//            
//        }
//        
//        
//        
//        
//        
//    }
//        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
//        {
//            self.view.endEditing(true)
//        }
//        
//        func textFieldDidEndEditing(_ textField: UITextField) {
//            
//        }
//    
//    
//    @IBAction func btnBackButton(_ sender: Any) {
//        
//         _ = self.navigationController?.popViewController(animated: true)
//    }
//    
//    
//    func bottomBorder(textFieldName: UITextField,placeHolder:String)
//    {
//        let color = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 0.8)
//        let border = CALayer()
//        let width = CGFloat(1)
//        border.borderColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 0.8).cgColor
//        border.frame = CGRect(x: 0, y: textFieldName.frame.size.height - width+10, width:  textFieldName.frame.size.width+50, height: textFieldName.frame.size.height)
//        
//        border.borderWidth = width
//        textFieldName.layer.addSublayer(border)
//        textFieldName.layer.masksToBounds = true
//        textFieldName.attributedPlaceholder = NSAttributedString(string: placeHolder,
//                                                                 attributes: [NSForegroundColorAttributeName: color ])
//        
//    }
//    
//    
//    
//}
//
//
