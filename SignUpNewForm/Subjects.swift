//
//  Subjects.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import ObjectMapper

class Subjects: Mappable{
    var subject_id: Int?
    var subject_name: String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        subject_id <- map["subject_id"]
        subject_name <- map["subject_name"]
    }
}
