//
//  Data.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import ObjectMapper

class IData: Mappable{
    var date: String?
    var date1: String?
    var day: String?
    var details: [Details]?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        date <- map["date"]
        date1 <- map["date1"]
        day <- map["day"]
        details <- map["details"]
    }
}
