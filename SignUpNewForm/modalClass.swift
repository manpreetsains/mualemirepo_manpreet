//
//  modalClass.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    var statusCode: Int?
    var msg: String?
    var profile: Profile?
    
    required init?(map: Map){
        statusCode <- map["status_code"]
        msg <- map["msg"]
        profile <- map["profile"]
    }
    
    func mapping(map: Map){
        statusCode <- map["status_code"]
        msg <- map["msg"]
        profile <- map["profile"]
    }
}

//["username": txtName.text,"email":txtEmail.text, "password":txtPassword.text, "phone":txtPhoneNo.text,"country":txtCountry.text,"city":txtCity.text,"address":address.text, "flag":"1"]
class Profile: Mappable {
    var username: String?
    var phone: String?
    var email: String?
    var password: String?
    var country:String?
    var city:String?
    var address:String?
    var birthday:String?
    
    required init?(map: Map){
        username <- map["username"]
        phone <- map["phone"]
        email <- map["email"]
        password <- map["password"]
        country <- map["country"]
        city <- map["city"]
        address <- map["address"]
        birthday <- map["birthday"]
        
        
        
    }
    func mapping(map: Map){
        username <- map["username"]
        phone <- map["phone"]
        email <- map["email"]
        password <- map["password"]
        country <- map["country"]
        city <- map["city"]
        address <- map["address"]
        birthday <- map["birthday"]
        
    }
}
