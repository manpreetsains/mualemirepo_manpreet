//
//  Locations.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import ObjectMapper

class Locations: Mappable{
    var slot_day: String?
    var slot_location: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        slot_day <- map["slot_day"]
        slot_location <- map["slot_location"]
    }
}
