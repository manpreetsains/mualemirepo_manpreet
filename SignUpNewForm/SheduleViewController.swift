//
//  SheduleViewController.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class SheduleViewController: UIViewController {
    var userModel:Instructor?
    var array:[IData] = []
    var data:IData?
    let date = Date()
    var distanceTotal = 0
    let formatter = DateFormatter()
    var initialConstraints = [NSLayoutConstraint]()
     var currentdate = " "
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var day1: UILabel!
    
    @IBOutlet weak var day2: UILabel!
    
    @IBOutlet weak var day3: UILabel!
    
    @IBOutlet weak var day4: UILabel!
    
    @IBOutlet weak var day5: UILabel!
    
    @IBOutlet weak var day6: UILabel!
    
    @IBOutlet weak var day7: UILabel!
    
    @IBOutlet weak var indicator: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        formatter.dateFormat = "yyyy-MM-dd"
        currentdate = formatter.string(from: date)
        self.lblDate.text = currentdate
        indicator.translatesAutoresizingMaskIntoConstraints = false
        fetchData()
     
        
    }
    
    
    func fetchData() {
        
        
        let param:[String:Any] = ["access_token":"ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8","page_no": 1 ,"page_size":7,"date_selected":currentdate]
        
        ApiHandler.fetchData( parameters: param)
        { (jsonData) in
            print(jsonData!)
             self.data = Mapper<IData>().map(JSONObject: jsonData)
            self.userModel = Mapper<Instructor>().map(JSONObject: jsonData)
           
            
            print((self.userModel?.data?[0].details?[0].subjects?.count)!, "")
            print(self.userModel?.data?.count ??  0)
            self.collectionView.reloadData()
            //            print(userModel?.data?[0].details?[0].age_group ?? "")
            var str = self.userModel?.data?[0].day
            var index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day1.text = str?.substring(to: index!)
            str = self.userModel?.data?[1].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day2.text = str?.substring(to: index!)
            str = self.userModel?.data?[2].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day3.text = str?.substring(to: index!)
            str = self.userModel?.data?[3].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day4.text = str?.substring(to: index!)
            str = self.userModel?.data?[4].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day5.text = str?.substring(to: index!)
            str = self.userModel?.data?[5].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day6.text = str?.substring(to: index!)
            str = self.userModel?.data?[6].day
            index = str?.index((str?.startIndex)!, offsetBy: 3)
            self.day7.text = str?.substring(to: index!)
            self.array = (self.userModel?.data)!
            print("---------",(self.userModel?.data)!)
            
//            last_date
        }
        
     }
    //forcast?.dailyForecasts?[indexPath.item].day?.iconPhrase ?? ""
}
extension SheduleViewController:UICollectionViewDataSource,UICollectionViewDelegate
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userModel?.data?.count ??  0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionViewCellController
        cell.details = (userModel?.data?[indexPath.row].details)!
        
        
        return cell
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var leadingConstraints:NSLayoutConstraint
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint)!
        formatter.dateFormat = "yyyy-MM-dd"
        currentdate = formatter.string(from: date)
        self.lblDate.text = array[visibleIndexPath[1]].date1
        let distance = visibleIndexPath[1]
      if distance == 1
        {
         distanceTotal = distanceTotal + 60
           leadingConstraints = indicator.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant:CGFloat(distanceTotal))
            NSLayoutConstraint.activate([leadingConstraints])
        }
       
        else if distance == 2
        {
       
        distanceTotal = distanceTotal + 120
        leadingConstraints = indicator.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant:CGFloat(distanceTotal))
            
        NSLayoutConstraint.activate([leadingConstraints])
        }
        
        
        print(visibleIndexPath[1])
    }
    
}

//
//ApiHandler.fetchData(parameters: param) { (jsonData) in
//    // print(jsonData)
//    let userModel = Mapper<Abhi>().map(JSONObject: jsonData)
//    print(userModel)
//    self.apiMsg = userModel?.msg ?? ""
//    print(self.apiMsg)
//    
//    
//    self.array = (userModel?.data)!
//    self.collectionView.reloadData()
//    
//}
//}
//override func didReceiveMemoryWarning() {
//    super.didReceiveMemoryWarning()
//    
//}
//
//
//}


//extension ViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView,numberOfItemsInSection numberofItemsInSection:Int)->Int {
//        return array.count
//    }
//    func  collectionView(_ collectionView: UICollectionView,cellForItemAt indexPath: IndexPath) ->UICollectionViewCell{
//        guard let cell:CollectionViewCell1 = (collectionView.dequeueReusableCell(withReuseIdentifier: "identifier1", for: indexPath) as? CollectionViewCell1)else{return CollectionViewCell1()}
//        cell.cellData = array[indexPath.row].details
//        
//        
//        return cell
//        
//}
