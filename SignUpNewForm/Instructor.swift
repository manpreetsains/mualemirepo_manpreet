//
//  Instructor.swift
//  SignUpNewForm
//
//  Created by Sierra 4 on 25/02/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import ObjectMapper

class Instructor: Mappable{
    var update_popup: Int?
    var status_code: Int?
    var is_package: Int?
    var data: [IData]?
    var msg: String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        update_popup <- map["update_popup"]
        status_code <- map["status_code"]
        is_package <- map["is_package"]
        data <- map["data"]
        msg <- map["msg"]
    }
}













